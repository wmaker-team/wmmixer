wmmixer (1.9-2) unstable; urgency=medium

  [ Doug Torrance ]
  * debian/control
    - Update Maintainer email address to use tracker.d.o.
    - Bump Standards-Version to 4.6.0.
  * debian/copyright
    - Update upstream mailing list address.
    - Use https in Source URL.
    - Add myself to copyright holders for debian/*.
  * debian/salsa-ci.yml
    - Add Salsa pipeline config file.
  * debian/upstream/metadata
    - Add DEP-12 upstream metadata file.

  [ Debian Janitor ]
  * debian/changelog
    - Fix day-of-week for changelog entries 1.5-12, 0.2-1.
  * debian/control
    - Use secure URI in Homepage field.

 -- Doug Torrance <dtorrance@piedmont.edu>  Tue, 26 Oct 2021 07:19:17 -0400

wmmixer (1.9-1) unstable; urgency=medium

  [ Doug Torrance ]
  * d/control:
    - Update Vcs-* after migration to Salsa.
  * d/watch:
    - Use https for download link.

  [ Jeremy Sowden ]
  * New upstream version 1.9 (closes: #984408).
  * d/compat, d/control
    - Remove compat and add a build-dep on `debhelper-compat = 13` to control.
  * d/control
    - Bump Standards-Version to 4.5.1.
    - Set Rules-Requires-Root: no.
    - Add myself to uploaders.
  * d/gbp.conf
    - Set dch commit-message format.
  * d/changelog
    - Remove trailing white-space.

 -- Jeremy Sowden <jeremy@azazel.net>  Tue, 17 Aug 2021 11:32:02 +0100

wmmixer (1.8-1) unstable; urgency=medium

  * New maintainer (Closes: #866848).
  * New upstream release.
  * debian/compat
    - Use debhelper compatibility level 10.
  * debian/control
    - Bump versioned depencency on debhelper to >= 10.
    - Add pkg-config to Build-Depends.
    - Bump Standards-Version to 4.1.0.
    - Update Homepage and Vcs-*.
    - Sort Depends alphabetically.
  * debian/copyright
    - Update Format, Upstream-Contact, and Source.
  * debian/docs
    - Rename from wmmixer.docs for simplicity.
  * debian/examples
    - Rename from wmmixer.examples for simplicity.
  * debian/home.mixer
    - Add example file to debian directory; was forgotten with new
      upstream release.
  * debian/menu
    - Remove deprecated Debian menu file.
  * debian/patches
    - Remove patches; no longer necessary now that upstream uses
      autotools for build.
  * debian/rules
    - Remove DH_VERBOSE comment line.
    - Use all hardening flags.
  * debian/watch
    - Use new download location.
  * debian/wmmixer.changelogs
    - Remove useless file.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 30 Aug 2017 00:07:21 -0400

wmmixer (1.7+20120605-1) unstable; urgency=low

  * New upstream release (from git).
    - Updates the FSF address only, no code changes.
  * debian/control
    - Updated maintainer email address from kix@kix.es to kix@debian.org.
    - Bump Standards-Version to 3.9.4.
    - Debhelper compatibility bumped to 9.
    - Added Vcs-Git and Vcs-Browser fields.
  * debian/copyright
    - Removed the string "Upstream-Contact:" in the second upstream contact
      name, to avoid lintian warnings.
  * New patch debian/patches/03_Makefile_hardening.patch to include the Debian
    Hardening flags in the compiler process.

 -- Rodolfo García Peñas (kix) <kix@debian.org>  Sun, 21 Jul 2013 18:03:00 +0200

wmmixer (1.7-1) unstable; urgency=low

  * New upstream version 1.7
    - Fix FTBFS with ld --as-needed
      Patch sent by Angel Abad <angelabad@ubuntu.com> [Closes: #640301]
  * debian/copyright is now DEP-5
  * debian/patches/* are now DEP-3

 -- Rodolfo García Peñas (kix) <kix@kix.es>  Mon, 19 Sep 2011 15:29:32 +0200

wmmixer (1.6-1) unstable; urgency=low

  * New upstream version 1.6
  * Four Debian patches are now in the upstream. Deleted form debian/patches
    - 01_restore_pristine_code.patch
    - 02_multiple_config_files.patch
    - 03_hurd_support.patch
    - 04_xclass_support.patch
  * Debian debian/patches renamed
    - 05_debian_paths.patch to 01_debian_paths.patch
    - 06_Makefile_optimization.patch to 02_Makefile_optimization.patch
  * Better application info in control file

 -- Rodolfo García Peñas (kix) <kix@kix.es>  Wed, 13 Jul 2011 12:08:43 +0200

wmmixer (1.5-12) unstable; urgency=low

  * Support multiple config files and X Classes [Closes: #454286]
  * Debian standards version moved to 3.9.2 (from 3.8.0) and compatibility to 7
  * deleted README.source
  * dpatch removed
  * Hurd support
  * Switch to dpkg-source 3.0 (quilt) format
  * Package adoption. New Maintainer [Closes: #490739]

 -- Rodolfo García Peñas (kix) <kix@kix.es>  Fri, 08 Apr 2011 23:38:05 +0200

wmmixer (1.5-11) unstable; urgency=low

  * QA upload.
  * debian/control
    - set QA Group as maintainer
    - added depends on oss-compat; Closes: #503846
    - bump Standards-Version to 3.8.0
      + added debian/README.source

 -- Sandro Tosi <morph@debian.org>  Wed, 29 Oct 2008 14:03:45 +0100

wmmixer (1.5-10) unstable; urgency=low

  * debian/control
    - bump Standard-Version to 3.7.3
  * debian/copyright
    - upstream authors, copyright and license indented with 4 spaces

 -- Sandro Tosi <matrixhasu@gmail.com>  Sun, 06 Jan 2008 01:23:22 +0100

wmmixer (1.5-9) unstable; urgency=low

  * Adopting package (Closes: #427083)
  * debian/control
    - set myself as new maintainer
    - little reformat of short and long descriptions
    - added dependency against dpatch
  * debian/copyright
    - set myself as new maintainer
    - added upstream author
    - added copyright owners information
    - added license information
    - added license file location on a debian system
  * debian/menu
    - updated menu section
  * debian/rules
    - very little fixes installed filenames formattation
    - integrated dpatch
  * debian/patches/01_restore_pristine_code.dpatch
    - added to remove upstream code changes
  * {mixctl.h,exception.h,xhandler.cc,mixctl.cc,README}
    - restored to upstream version

 -- Sandro Tosi <matrixhasu@gmail.com>  Mon, 08 Oct 2007 21:38:57 +0200

wmmixer (1.5-8) unstable; urgency=medium

  * QA upload.
  * Acknowledge NMU.  Closes: #174899.
  * xhandler.cc: Initialize `icon_list_'.  Thanks to Florent Bayle for
    the patch.  Closes: #429509.
  * Remove no longer necessary build dependency on x-dev.
  * Switch to debhelper 5.
  * debian/rules:
    - Add support for DEB_BUILD_OPTIONS=noopt.
    - Remove support for DEB_BUILD_OPTIONS=debug.
    - Let dh_strip handle DEB_BUILD_OPTION=nostrip.
  * Conforms to Standards version 3.7.2.

 -- Matej Vela <vela@debian.org>  Mon, 18 Jun 2007 21:24:01 +0200

wmmixer (1.5-7) unstable; urgency=low

  * Orphan package, set maintainer to Debian QA Group

 -- Gordon Fraser <gordon@debian.org>  Fri, 01 Jun 2007 21:09:47 +0200

wmmixer (1.5-6.1) unstable; urgency=low

  * Porter NMU.
  * Added support for GNU/kFreeBSD (Closes: #332992).

 -- Aurelien Jarno <aurel32@debian.org>  Sun, 10 Dec 2006 21:45:54 +0100

wmmixer (1.5-6) unstable; urgency=low

  * Fix build dependencies (Closes: #346893)

 -- Gordon Fraser <gordon@debian.org>  Sun, 15 Jan 2006 19:20:10 +0100

wmmixer (1.5-5) unstable; urgency=low

  * Rebuild for g++ transition

 -- Gordon Fraser <gordon@debian.org>  Mon,  3 Oct 2005 14:30:09 +0200

wmmixer (1.5-4) unstable; urgency=low

  * Added missing dot to package description (Closes: #174899)

 --  <gordon@debian.org>  Thu, 29 Jul 2004 08:38:02 +0200

wmmixer (1.5-3) unstable; urgency=low

  * gcc-3.x updates to fix FTBFS (Closes: #218942)
    (Thanks, Frank Lichtenheld)

 -- Gordon Fraser <gordon@debian.org>  Tue, 11 Nov 2003 07:51:21 +0100

wmmixer (1.5-2) unstable; urgency=low

  * Fix documentation error (Closes: #151385)

 -- Gordon Fraser <gordon@debian.org>  Sat, 29 Jun 2002 19:16:55 +0200

wmmixer (1.5-1) unstable; urgency=low

  * New upstream version

 -- Gordon Fraser <gordon@debian.org>  Tue, 25 Jun 2002 22:14:11 +0200

wmmixer (1.4-1) unstable; urgency=low

  * New upstream version

 -- Gordon Fraser <gordon@debian.org>  Sat,  8 Jun 2002 18:13:17 +0200

wmmixer (1.3-3) unstable; urgency=low

  * Fix build failure on ia64 (Closes: #148454)

 -- Gordon Fraser <gordon@debian.org>  Wed, 29 May 2002 11:29:51 +0200

wmmixer (1.3-2) unstable; urgency=low

  * Fix segfault on reading configuration file (Closes: #148245)
  * Support DEB_BUILD_OPTIONS

 -- Gordon Fraser <gordon@debian.org>  Mon, 27 May 2002 09:28:24 +0200

wmmixer (1.3-1) unstable; urgency=low

  * New upstream version
  * Fix build failure on hppa (Closes: #148196)

 -- Gordon Fraser <gordon@debian.org>  Sun, 26 May 2002 13:18:59 +0200

wmmixer (1.2-1) unstable; urgency=low

  * New upstream version

 -- Gordon Fraser <gordon@debian.org>  Sat, 25 May 2002 15:01:15 +0200

wmmixer (1.1-4) unstable; urgency=low

  * Fix version string
  * Improve mousewheel behaviour for changing both channels

 -- Gordon Fraser <gordon@debian.org>  Sun, 14 Apr 2002 15:52:40 +0200

wmmixer (1.1-3) unstable; urgency=low

  * Added ioctl to check for changes before doing updates
    This reduces CPU load a _lot_
  * Really give compiler CXXFLAGS
  * Get rid of compiler warnings

 -- Gordon Fraser <gordon@debian.org>  Fri, 12 Apr 2002 09:13:37 +0200

wmmixer (1.1-2) unstable; urgency=low

  * Correct Makefile to use C++ compiler instead of C compiler
    (Closes: #141696)

 -- Gordon Fraser <gordon@debian.org>  Mon,  8 Apr 2002 09:00:48 +0200

wmmixer (1.1-1) unstable; urgency=low

  * New maintainer
  * New upstream release, new upstream author - me
  * Add manpage (Closes: #93477)
  * Add wheelmouse support (Closes: #47984)
  * Move executable to /usr/bin (Closes: #122022)
  * Update menu entry (Closes: #82330)
  * Update X handling, now works with KDE Dock App Bar (Closes: #108529)
  * It builds now (new Makefile, without imake), change Build-Depends

 -- Gordon Fraser <gordon@debian.org>  Tue,  2 Apr 2002 19:36:36 +0200

wmmixer (1.0beta1-6.1) unstable; urgency=low

  * NMU
  * Add Build-Depends: xutils.  Closes: #105305

 -- LaMont Jones <lamont@smallone.fc.hp.com>  Sat, 14 Jul 2001 23:16:14 -0600

wmmixer (1.0beta1-6) unstable; urgency=low

  * Can now handle as many channels as your sound card can.
  * cleaned up menu file, added a non-docked entry
  * Closes: #52908

 -- Sean 'Shaleh' Perry <shaleh@debian.org>  Fri,  6 Oct 2000 10:46:24 -0700

wmmixer (1.0beta1-5) unstable; urgency=low

  * New maintainer

 -- Sean E. Perry <shaleh@debian.org>  Fri,  3 Sep 1999 14:30:29 -0700

wmmixer (1.0beta1-4) unstable; urgency=low

  * New maintainer

 -- Neale Pickett <neale@debian.org>  Thu, 25 Feb 1999 14:30:29 -0700

wmmixer (1.0beta1-3) frozen unstable; urgency=low

  * debian/rules: added dh_installexamples home.wmmixer (closes: bug#25482)
  * recompiled with lastest g++
  * Added frozen to the target distributions...

 -- Marcelo E. Magallon <mmagallo@debian.org>  Fri,  4 Dec 1998 13:58:26 -0600

wmmixer (1.0beta1-2) unstable; urgency=low

  * debian/rules: added dh_installexamples home.wmmixer (closes: bug#25482)
  * recompiled with lastest g++

 -- Marcelo E. Magallon <mmagallo@debian.org>  Fri,  4 Dec 1998 12:36:31 -0600

wmmixer (1.0beta1-1) unstable; urgency=low

  * New upstream version
  * Added CC=c++ to debian/rules (why does it work ok with wmmount but not
    with this one?)
  * debian/rules: changed cp wmmixer ... to install -s -m 0755 wmmixer ...
  * debian/control: upgraded Standards Version to 2.4.1
  * debian/control: Suggests: wmaker | afterstep
  * debian/rules: nuke -DX_LOCALE after making Makefile
  * Added copyright information to debian/rules

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sun,  4 Oct 1998 20:42:22 -0600

wmmixer (0.8-1) frozen unstable; urgency=low

  * New upstream version
  * WRT version 0.2 this: fixes bug relating to manual window positioning;
    reduces CPU load; fixes bug relating to cards with no volume channel;
    has better checking of supported channels; is GPLed.

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sat,  9 May 1998 21:33:48 -0600

wmmixer (0.7-1) unstable; urgency=low

  * New upstream version. It's GPL'ed now! Moving into main
  * Applied patch that handles Expose events

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sat,  2 May 1998 13:37:54 -0600

wmmixer (0.6-1) unstable; urgency=low

  * New upstream version

 -- Marcelo E. Magallon <mmagallo@debian.org>  Wed,  8 Apr 1998 21:20:33 -0600

wmmixer (0.2-2) frozen unstable; urgency=low

  * Moving to non-free due to copyright

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sat, 21 Mar 1998 15:29:04 -0600

wmmixer (0.2-1) unstable; urgency=low

  * Initial release

 -- Marcelo E. Magallon <mmagallo@debian.org>  Thu, 05 Mar 1998 23:01:10 -0600
