Source: wmmixer
Section: x11
Priority: optional
Maintainer: Debian Window Maker Team <team+wmaker@tracker.debian.org>
Uploaders: Doug Torrance <dtorrance@piedmont.edu>,
           Jeremy Sowden <jeremy@azazel.net>
Build-Depends: debhelper-compat (= 13),
               libx11-dev,
               libxext-dev,
               libxpm-dev,
               pkg-config
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://www.dockapps.net/wmmixer
Vcs-Browser: https://salsa.debian.org/wmaker-team/wmmixer
Vcs-Git: https://salsa.debian.org/wmaker-team/wmmixer.git

Package: wmmixer
Architecture: any
Depends: oss-compat, ${misc:Depends}, ${shlibs:Depends}
Suggests: wmaker | afterstep
Description: mixer application designed for WindowMaker
 wmmixer displays the mixer status of your computer in a small icon.
 Most common channels are identified with an appropriate icon. Control
 include a stereo (mono where appropriate) volume control and a recording
 source toggle button.
 .
 It is possible to change between the different channels using two small
 buttons (previous channel, next channel) and to change the volume of the
 channel.
 .
 There's nothing in the program that makes it require WindowMaker, except maybe
 the NeXTStep look and the fact that it properly docks. It can be used with
 other window managers without problems.
